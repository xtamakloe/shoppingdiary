package com.artoconnect.ubicompv2.adapters;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.artoconnect.ubicompv2.app.R;
import com.artoconnect.ubicompv2.models.Item;

import java.util.List;

/* Custom Adapter for Items list view */
public class ItemsListAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null; // View inflater
    private Item tempValues;
    private Activity mActivity;
    private List<Item> mItems;
    private boolean mBasketItem;

    public ItemsListAdapter(Activity activity,
                                 List<Item> items, boolean basketItem) {
        mActivity = activity;
        mItems = items;
        mBasketItem = basketItem;
        inflater = (LayoutInflater)
                mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        if (mItems.size() <= 0)
            return 1;
        return mItems.size();
    }

    public Object getItem(int position) {
        return mItems.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;
        holder = new ViewHolder();

        if (convertView == null) {
            if  (mBasketItem) {
                vi = inflater.inflate(R.layout.basket_item_row, null);
            } else {
                vi = inflater.inflate(R.layout.item_row, null);
            }

            holder.name = (TextView) vi.findViewById(R.id.tv_item_name);
            if (mBasketItem)
                holder.quantity = (TextView) vi.findViewById(R.id.tv_basket_item_quantity);

            vi.setTag(holder);
        } else
            holder = (ViewHolder) vi.getTag();

        if (mItems.size() <= 0) {
            holder.name.setText("");
        } else {
            tempValues = null;
            tempValues = mItems.get(position);

            holder.name.setText(tempValues.getName());

            if (mBasketItem)
                holder.quantity.setText(Integer.toString(tempValues.getQuantity()));
        }
        return vi;
    }

    public static class ViewHolder {
        public TextView name;
        public TextView quantity;
    }
}