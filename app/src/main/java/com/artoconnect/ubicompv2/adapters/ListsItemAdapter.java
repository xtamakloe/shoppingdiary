package com.artoconnect.ubicompv2.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.artoconnect.ubicompv2.app.R;
import com.artoconnect.ubicompv2.models.ShoppingList;

import java.util.List;

/* Custom Adapter for Lists list view */
public class ListsItemAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    private ShoppingList tempValues;
    private Activity mActivity;
    private List<ShoppingList> mListItems;


    public ListsItemAdapter(Activity activity, List<ShoppingList> items) {
        mActivity = activity;
        mListItems = items;

        inflater = (LayoutInflater)
                mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        if (mListItems.size() <= 0)
            return 1;
        return mListItems.size();
    }

    public Object getItem(int position) {
        return mListItems.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;
        holder = new ViewHolder();

        if (convertView == null) {

            vi = inflater.inflate(R.layout.shopping_list_item_row, null);

            holder.name = (TextView)
                    vi.findViewById(R.id.tv_shopping_list_name);
            holder.item_count = (TextView)
                    vi.findViewById(R.id.tv_shopping_list_item_count);
            vi.setTag(holder);
        } else
            holder = (ViewHolder) vi.getTag();

        if (mListItems.size() <= 0) {
            holder.name.setText("");
        } else {
            tempValues = null;
            tempValues = mListItems.get(position);

            holder.name.setText(tempValues.getName());
            holder.item_count.setText("Number of Items: " +
                    Integer.toString(tempValues.getItem_count()));
        }
        return vi;
    }

    public static class ViewHolder {
        public TextView name;
        public TextView item_count;
    }
}
