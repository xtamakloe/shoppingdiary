package com.artoconnect.ubicompv2.adapters;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.artoconnect.ubicompv2.app.R;
import com.artoconnect.ubicompv2.models.MasterItem;

import java.util.List;

/* Custom Adapter for MasterItems list view */
public class MasterItemsListAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    private MasterItem tempValues;
    private Activity mActivity;
    private List<MasterItem> mItems;

    public MasterItemsListAdapter(Activity activity, List<MasterItem> items) {
        mActivity = activity;
        mItems = items;
        inflater = (LayoutInflater)
                mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        if (mItems.size() <= 0)
            return 1;
        return mItems.size();
    }

    public Object getItem(int position) {
        return mItems.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;
        holder = new ViewHolder();

        if (convertView == null) {

            vi = inflater.inflate(R.layout.item_row, null);

            holder.name = (TextView) vi.findViewById(R.id.tv_item_name);

            vi.setTag(holder);
        } else
            holder = (ViewHolder) vi.getTag();

        if (mItems.size() <= 0) {
            holder.name.setText("");
        } else {
            tempValues = null;
            tempValues = mItems.get(position);

            holder.name.setText(tempValues.getName());
        }
        return vi;
    }

    public static class ViewHolder {
        public TextView name;
    }
}