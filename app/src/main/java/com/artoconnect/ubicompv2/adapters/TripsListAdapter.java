package com.artoconnect.ubicompv2.adapters;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.artoconnect.ubicompv2.app.R;
import com.artoconnect.ubicompv2.models.Trip;

import java.util.List;

/* Custom Adapter for Trips list view */
public class TripsListAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    private Trip tempValues;
    private Activity mActivity;
    private List<Trip> mTrips;

    public TripsListAdapter(Activity activity, List<Trip> trips) {
        this.mActivity = activity;
        mTrips = trips;

        inflater = (LayoutInflater)
                this.mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        if (mTrips.size() <= 0)
            return 1;
        return mTrips.size();
    }

    public Object getItem(int position) {
        return mTrips.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;

        if (convertView == null) {
            vi = inflater.inflate(R.layout.trip_item, null);

            holder = new ViewHolder();
            holder.name = (TextView) vi.findViewById(R.id.tv_trip_store_name);
            holder.timestamp = (TextView) vi.findViewById(R.id.tv_trip_timestamp);

            vi.setTag(holder);
        } else
            holder = (ViewHolder) vi.getTag();

        if (mTrips.size() <= 0) {
            holder.name.setText("");
        } else {
            tempValues = null;
            tempValues = mTrips.get(position);

            holder.name.setText(tempValues.getStore().getName());
            holder.timestamp.setText(tempValues.getTimestamp());
        }
        return vi;
    }

    public static class ViewHolder {
        public TextView name;
        public TextView timestamp;
    }


}