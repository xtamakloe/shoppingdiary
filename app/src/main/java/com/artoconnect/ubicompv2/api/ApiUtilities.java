package com.artoconnect.ubicompv2.api;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/* Global class to hold shared API methods */
public class ApiUtilities {

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = cMgr.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
