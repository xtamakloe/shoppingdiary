package com.artoconnect.ubicompv2.api;

import com.artoconnect.ubicompv2.models.ih.Product;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Query;

/* Class to handle communication with InvisibleHand API for product information*/
public class ProductApiClient {

    private static final String AUTH = "app_id=4dd1354c" +
                                       "&app_key=967c90ecfff15996e2cff289e4a48ea5";
    private static ProductApiInterface sProductService;

    public static ProductApiInterface getProductApiClient() {
        if (sProductService == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setServer("http://uk.api.invisiblehand.co.uk/v1")
                    .build();
            sProductService = restAdapter.create(ProductApiInterface.class);
        }
        return sProductService;
    }

    public interface ProductApiInterface {
        @GET("/products?" + AUTH)
        void getProduct(@Query("identifier") String id, Callback<Product> callback);
    }

}
