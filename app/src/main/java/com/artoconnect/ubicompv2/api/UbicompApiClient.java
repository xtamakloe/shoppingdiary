package com.artoconnect.ubicompv2.api;


import com.artoconnect.ubicompv2.models.Item;
import com.artoconnect.ubicompv2.models.ShoppingList;
import com.artoconnect.ubicompv2.models.Trip;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/* Handles communication with Ubicomp back-end server API */
public class UbicompApiClient {
    private static UbicompApiInterface sUbicompService;

    public static UbicompApiInterface getUbicompApiClient() {
        if (sUbicompService == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setServer("http://ubicompv2.herokuapp.com/api/v1")
                    .build();
            sUbicompService = restAdapter.create(UbicompApiInterface.class);
        }
        return sUbicompService;
    }

    public interface UbicompApiInterface {
        /*
        * Trips
        * */
        @GET("/trips.json")
        void getTrips(@Query("user_id") int userId,
                      @Query("limit") int limit,
                      @Query("offset") int offset,
                      Callback<List<Trip>> tripsCallback);

        @GET("/trips/{trip_id}.json")
        void getTrip(@Query("user_id") int userId,
                      @Path("trip_id") int tripId,
                      Callback<Trip> tripCallback);

        @POST("/trips.json")
        void addTrip(@Query("user_id") int userId,
                     @Query("store_uid") String storeUid,
                     @Query("store_name") String storeName,
                     Callback<Trip> tripCallback);

        @PUT("/trips/{trip_id}.json")
        void updateTrip(@Query("user_id") int userId,
                        @Path("trip_id") int tripId,
                        @Query("complete") String complete,
                        Callback<Trip> tripCallback);

        @DELETE("/trips/{trip_id}.json")
        void cancelTrip(@Query("user_id") int userId,
                        @Path("trip_id") int tripId,
                        Callback<Boolean> boolCallback);

        /*
        * Trip Items
        * */
        @POST("/trips/{trip_id}/items.json")
        void addTripItem(@Query("user_id") int userId,
                         @Path("trip_id") int tripId,
                         @Query("uid") String identifier,
                         @Query("name") String name,
                         @Query("quantity") String quantity,
                         Callback<Item> itemCallback);

        @GET("/trips/{trip_id}/items.json")
        void getTripItems(@Query("user_id") int userId,
                          @Path("trip_id") int tripId,
                          @Query("limit") int limit,
                          @Query("offset") int offset,
                          Callback<List<Item>> itemsCallback);

        /*
        * Master Items
        * */
        @GET("/master_items.json")
        void getItems(@Query("user_id") int userId,
                      @Query("limit") int limit,
                      @Query("offset") int offset,
                      Callback<List<Item>> itemsCallback);

        /*
        * Lists
        * */

         @GET("/lists.json")
        void getLists(@Query("user_id") int userId,
                      @Query("limit") int limit,
                      @Query("offset") int offset,
                      Callback<List<ShoppingList>> listCallback);

        @GET("/lists/{list_id}.json")
        void getList(@Query("user_id") int userId,
                     @Path("list_id") int listId,
                     Callback<ShoppingList> listCallback);

        @POST("/lists.json")
        void addList(@Query("user_id") int userId,
                     @Query("name") String listName,
                     Callback<ShoppingList> listCallback);

        @PUT("/lists/{list_id}/items/add.json")
        void addItemToList(@Query("user_id") int userId,
                           @Path("list_id") int listId,
                           @Query("item_uid") String itemUid,
                           Callback<ShoppingList> listCallback);

        void removeItemFromList(@Query("user_id") int userId,
                               @Path("list_id") int listId,
                               Callback<Boolean> boolCallback);

        @DELETE("/lists/{list_id}.json")
        void deleteList(@Query("user_id") int userId,
                        @Path("list_id") int listId,
                        Callback<Boolean> boolCallback);
    }
}