package com.artoconnect.ubicompv2.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.artoconnect.ubicompv2.adapters.ItemsListAdapter;
import com.artoconnect.ubicompv2.api.ApiUtilities;
import com.artoconnect.ubicompv2.api.UbicompApiClient;
import com.artoconnect.ubicompv2.models.Item;
import com.artoconnect.ubicompv2.models.Trip;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class BasketActivity extends ActionBarActivity {

    static final int SCAN_ITEMS_REQUEST = 1;

    public BasketActivity mBasketActivity = null;
    private ListView mListView;
    private ItemsListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket);

        mBasketActivity = this;
        Globals.sItems = new ArrayList<Item>();

        mListView = (ListView) mBasketActivity.findViewById(R.id.list);
        mAdapter = new ItemsListAdapter(mBasketActivity, Globals.sItems, true);
        mListView.setAdapter(mAdapter);

        final ImageButton scanItem = (ImageButton) findViewById(R.id.btn_scan_items);

        scanItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),
                        ScannerActivity.class);
                startActivityForResult(intent, SCAN_ITEMS_REQUEST);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SCAN_ITEMS_REQUEST) {
            if (resultCode == RESULT_OK) {
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.basket, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case R.id.action_accept:
                endTrip(true);
                return true;
            case R.id.action_cancel:
                Toast.makeText(getApplicationContext(), "false", Toast.LENGTH_SHORT).show();
                endTrip(false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        return;
    }

    private void endTrip(boolean save) {
        final int tripId = Globals.sTripId;
        Globals.sTripId = null;
        Globals.sItems = null;

        if (save) {
            // Make API call to save trip
            saveTrip(tripId);
        } else {
            // Make API call to destroy trip
            cancelTrip(tripId);
        }
        finish();
    }

    private void cancelTrip(int tripId) {
        if (ApiUtilities.isNetworkAvailable(getApplicationContext())) {
            UbicompApiClient.getUbicompApiClient().
                    cancelTrip(Globals.sUserId, tripId, new Callback<Boolean>() {
                        @Override
                        public void success(Boolean aBoolean, Response response) {

                        }

                        @Override
                        public void failure(RetrofitError retrofitError) {

                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(),
                    "You need a working internet connection to continue",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void saveTrip(final int tripId) {

        if (ApiUtilities.isNetworkAvailable(getApplicationContext())) {
            UbicompApiClient.getUbicompApiClient().updateTrip(
                    Globals.sUserId, tripId, "true", new Callback<Trip>() {
                        @Override
                        public void success(Trip trip, Response response) {
                            Intent intent = new Intent(
                                    getApplicationContext(), TripActivity.class);
                            intent.putExtra("tripId", tripId);
                            startActivity(intent);
                        }
                        @Override
                        public void failure(RetrofitError error) {}
                    }
            );
        } else {
            Toast.makeText(getApplicationContext(),
                    "You need a working internet connection to continue",
                    Toast.LENGTH_SHORT).show();
        }
    }
}

// TODO: proceed to basket activity, override back button function on basket activity
// only ways to leave basket activity should be controlled by me:
// which is cancel (destroy trip api) or done (save/update trip api)
