package com.artoconnect.ubicompv2.app;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.artoconnect.ubicompv2.api.ApiUtilities;
import com.artoconnect.ubicompv2.api.UbicompApiClient;
import com.artoconnect.ubicompv2.models.ShoppingList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class CreateListDialog extends DialogFragment {

    private CreateListDialogListener mListener;
    private Activity mActivity;

    public static CreateListDialog newInstance(
            Activity activity, CreateListDialogListener listener) {
        CreateListDialog fragment = new CreateListDialog();
        fragment.mListener = listener;
        fragment.mActivity = activity;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View layoutView = inflater.inflate(R.layout.dialog_create_list, null);

        // Pass null as the parent view because its going in the dialog layout
        builder.setView(layoutView)
            .setTitle("Enter a name for your list")
            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    // cancel and return to scanning
                    if (mListener != null)
                        mListener.onCreateListDialogClick(CreateListDialog.this);
                }
            })
            .setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    EditText et_list_name = (EditText)
                            layoutView.findViewById(R.id.et_list_name);
                    String name = et_list_name.getText().toString();

                    if (ApiUtilities.isNetworkAvailable(
                            getActivity().getApplicationContext())) {
                        // Add item product data from api and display popup
                        //TODO: Ensure sTripId and sUserId are set
                        UbicompApiClient.getUbicompApiClient().addList(
                                Globals.sUserId, name,
                                new Callback<ShoppingList>() {
                                    @Override
                                    public void success(ShoppingList list, Response response) {
                                        // add list to local cache
                                        //Globals.sLists.add(list);
                                        Toast.makeText(mActivity, "Added", Toast.LENGTH_SHORT).show();
                                        if (mListener != null)
                                            mListener.onCreateListDialogClick(CreateListDialog.this);
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        if (mListener != null)
                                            mListener.onCreateListDialogClick(CreateListDialog.this);
                                    }
                                }
                        );
                    }
                }
            });

        return builder.create();
    }


    public interface CreateListDialogListener {
        public void onCreateListDialogClick(DialogFragment dialog);
    }
}
