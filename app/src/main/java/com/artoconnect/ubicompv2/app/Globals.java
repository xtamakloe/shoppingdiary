package com.artoconnect.ubicompv2.app;

import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.artoconnect.ubicompv2.models.Item;
import com.artoconnect.ubicompv2.models.ShoppingList;

import java.util.List;

/*
* Temporary storage for session variables
* */
public class Globals {

    public static Integer BASKET_ACTIVITY_REQUEST = 0;
    public static Integer sUserId = 1; // current logged in user,
    public static Integer sTripId; // current trip, should be cleared on end of trip
    public static List<Item> sItems; // set when BasketActivity starts, cleared when trip is completed
    public static Boolean sRefreshTrips;
    public static List<ShoppingList> sLists;

    public static void refreshView(List<?> list, ListView listView, TextView emptyView) {
        if (list.size() > 0) {
            listView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        } else {
            listView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
    }
}
