package com.artoconnect.ubicompv2.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.artoconnect.ubicompv2.adapters.ItemsListAdapter;
import com.artoconnect.ubicompv2.api.ApiUtilities;
import com.artoconnect.ubicompv2.api.UbicompApiClient;
import com.artoconnect.ubicompv2.models.Item;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class ItemsFragment extends Fragment {
    ;
    private ListView mListView;
    private List<Item> mItems;
    private ItemsListAdapter mAdapter;
    private ProgressBar mProgressBar;
    private TextView mEmptyView;

    public ItemsFragment() {
        // Required empty public constructor
    }

    public static ItemsFragment newInstance() {
        ItemsFragment fragment = new ItemsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_items, container, false);

        mListView = (ListView) v.findViewById(R.id.lv_items_list);
        mProgressBar = (ProgressBar) v.findViewById(R.id.progressbar);
        mEmptyView = (TextView) v.findViewById(R.id.empty_items);

        mItems = new ArrayList<Item>();
        mAdapter = new ItemsListAdapter(getActivity(), mItems, false);
        mListView.setAdapter(mAdapter);

        loadItems();

        return v;
    }

    @Override
    public void onResume() {
        loadItems();
        super.onResume();
    }

    private void loadItems() {
        // show progressbar
        mProgressBar.setVisibility(View.VISIBLE);

        // api call to get items and populate listview
        if (ApiUtilities.isNetworkAvailable(getActivity().getApplicationContext())) {
            UbicompApiClient.getUbicompApiClient().getItems(
                    Globals.sUserId, 100, 0,
                    new Callback<List<Item>>() {
                        @Override
                        public void success(List<Item> items, Response response) {
                            // Hide progressbar and show list
                            mProgressBar.setVisibility(View.GONE);

                            mItems.clear();
                            mItems.addAll(items);
                            Globals.refreshView(mItems, mListView, mEmptyView);
                            mAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                        }
                    }
            );
        }
    }
}
