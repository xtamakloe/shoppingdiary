package com.artoconnect.ubicompv2.app;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.artoconnect.ubicompv2.adapters.ListsItemAdapter;
import com.artoconnect.ubicompv2.api.ApiUtilities;
import com.artoconnect.ubicompv2.api.UbicompApiClient;
import com.artoconnect.ubicompv2.models.ShoppingList;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ListsFragment extends Fragment implements
        CreateListDialog.CreateListDialogListener{

    private ListView mListView;
    private ProgressBar mProgressBar;
    private List<ShoppingList> mLists;
    private ListsItemAdapter mAdapter;
    private TextView mEmptyView;

    public ListsFragment() {
        // Required empty public constructor
    }

    public static ListsFragment newInstance() {
        ListsFragment fragment = new ListsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_lists, container, false);

        mListView = (ListView) v.findViewById(R.id.lv_shopping_lists_list);
        mProgressBar = (ProgressBar) v.findViewById(R.id.progressbar);
        mEmptyView = (TextView) v.findViewById(R.id.empty_lists);

        mLists = new ArrayList<ShoppingList>();
        mAdapter = new ListsItemAdapter(getActivity(), mLists);
        mListView.setAdapter(mAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView,
                                    View view, int position, long row) {
                Intent intent = new Intent(getActivity(), ShoppingListActivity.class);
                ShoppingList list = (ShoppingList) mAdapter.getItem(position);
                intent.putExtra("listId", list.getId());
                startActivity(intent);
            }
        });

        loadLists();

        final ImageButton addList = (ImageButton) v.findViewById(R.id.btn_add_list);
        addList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });
        return v;
    }

    private void showDialog() {
        DialogFragment fragment = CreateListDialog.newInstance(getActivity(), this);
        fragment.show(getActivity().getSupportFragmentManager(), "add_list");
    }

    @Override
    public void onResume() {
        loadLists();
        super.onResume();
    }

    @Override
    public void onCreateListDialogClick(DialogFragment dialog) {
        loadLists();
    }

    private void loadLists() {
        // show progressbar
        mProgressBar.setVisibility(View.VISIBLE);

        // api call to get items and populate listview
        if (ApiUtilities.isNetworkAvailable(getActivity().getApplicationContext())) {
            UbicompApiClient.getUbicompApiClient().getLists(
                    Globals.sUserId, 100, 0,
                    new Callback<List<ShoppingList>>() {
                        @Override
                        public void success(List<ShoppingList> lists,
                                            Response response) {
                            // Hide progressbar
                            mProgressBar.setVisibility(View.GONE);

                            mLists.clear();
                            mLists.addAll(lists);
                            Globals.refreshView(mLists, mListView, mEmptyView);
                            mAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(getActivity(), "Oh no", Toast.LENGTH_SHORT).show();
                        }
                    }
            );
        }
    }
}
