package com.artoconnect.ubicompv2.app;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.artoconnect.ubicompv2.api.ApiUtilities;
import com.artoconnect.ubicompv2.api.UbicompApiClient;
import com.artoconnect.ubicompv2.models.Item;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class ScanResultsDialogFragment extends DialogFragment {
    private boolean mSuccess;
    private String mTitle = "Scan Results";
    private String mItemName;
    private String mIdentifier;
    private ScanResultDialogListener mListener;

    public static ScanResultsDialogFragment newInstance(
        boolean success, String itemName, String identifier, ScanResultDialogListener listener
    ) {
        ScanResultsDialogFragment fragment = new ScanResultsDialogFragment();

        fragment.mSuccess = success;
        fragment.mItemName = itemName;
        fragment.mIdentifier = identifier;
        fragment.mListener = listener;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Determine type of dialog to display
        Integer layout;
        if (mSuccess) {
            layout = R.layout.dialog_scan_results_success;
        } else {
            layout = R.layout.dialog_scan_results_fail;
        }

        final View layoutView = inflater.inflate(layout, null);

        // Pass null as the parent view because its going in the dialog layout
        builder.setView(layoutView)
                .setTitle(mTitle)
                .setMessage(mItemName)
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // cancel and return to scanning
                        if (mListener != null)
                            mListener.onScanResultDialogClick(
                                    ScanResultsDialogFragment.this);
                    }
                });
        if (mSuccess) {
            builder.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    EditText et_quantity = (EditText)
                            layoutView.findViewById(R.id.et_quantity);
                    String qty = et_quantity.getText().toString();

                    if (ApiUtilities.isNetworkAvailable(
                            getActivity().getApplicationContext())) {
                        // Add item product data from api and display popup
                        //TODO: Ensure sTripId and sUserId are set
                        UbicompApiClient.getUbicompApiClient().addTripItem(
                                Globals.sUserId, Globals.sTripId, mIdentifier, mItemName, qty,
                                new Callback<Item>() {
                                    @Override
                                    public void success(Item item, Response response) {
                                        // if no errors return to scanning
                                        // if errors, tell user to scan again

                                        // add item to local cache
                                        Globals.sItems.add(item);
                                        Toast.makeText(getActivity(), "Added", Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        // Display error message to scan again,
                                        // cos invalid message was sent

                                        // don't add item to local cache
                                        // set some error variable to show unadded
                                    }
                                }
                        );
                    } else {
                        //showMessageDialog("You need a working internet connection to continue");
                    }
                    // Return to scanner
                    if (mListener != null)
                        mListener.onScanResultDialogClick(
                                ScanResultsDialogFragment.this);
                }
            });
        }
        return builder.create();
    }


    public interface ScanResultDialogListener {
        public void onScanResultDialogClick(DialogFragment dialog);
    }
}
