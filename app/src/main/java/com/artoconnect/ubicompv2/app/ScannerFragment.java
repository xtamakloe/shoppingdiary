package com.artoconnect.ubicompv2.app;


import android.app.Activity;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.artoconnect.ubicompv2.api.ApiUtilities;
import com.artoconnect.ubicompv2.api.ProductApiClient;
import com.artoconnect.ubicompv2.models.ih.Product;

import java.util.ArrayList;
import java.util.List;

import me.dm7.barcodescanner.zbar.BarcodeFormat;
import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class ScannerFragment extends Fragment implements
        MessageDialogFragment.MessageDialogListener,
        ZBarScannerView.ResultHandler,
        ScanResultsDialogFragment.ScanResultDialogListener {

    private static final String FLASH_STATE = "FLASH_STATE";
    private static final String AUTO_FOCUS_STATE = "AUTO_FOCUS_STATE";
    private static final String SELECTED_FORMATS = "SELECTED_FORMATS";
    private ZBarScannerView mScannerView;
    private boolean mFlash;
    private boolean mAutoFocus;
    private ArrayList<Integer> mSelectedIndices;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle state) {
        mScannerView = new ZBarScannerView(getActivity());
        if (state != null) {
            mFlash = state.getBoolean(FLASH_STATE, false);
            mAutoFocus = state.getBoolean(AUTO_FOCUS_STATE, true);
            mSelectedIndices = state.getIntegerArrayList(SELECTED_FORMATS);
        } else {
            mFlash = false;
            mAutoFocus = true;
            mSelectedIndices = null;
        }
        setupFormats();
        return mScannerView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem menuItem;
        menuItem = menu.add(Menu.NONE, R.id.menu_end_scan, 0, R.string.menu_end_scan);
        MenuItemCompat.setShowAsAction(menuItem, menuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.menu_end_scan:
                getActivity().setResult(Activity.RESULT_OK);
                getActivity().finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
        mScannerView.setFlash(mFlash);
        mScannerView.setAutoFocus(mAutoFocus);
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
        closeMessageDialog();
        closeFormatsDialog();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(FLASH_STATE, mFlash);
        outState.putBoolean(AUTO_FOCUS_STATE, mAutoFocus);
        outState.putIntegerArrayList(SELECTED_FORMATS, mSelectedIndices);
    }

    @Override
    public void handleResult(Result rawResult) {
        try {
            Uri notification = RingtoneManager.getDefaultUri(
                    RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(
                    getActivity().getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
        }

        final String identifier = rawResult.getContents();

        // If connected to the internet
        if (ApiUtilities.isNetworkAvailable(getActivity().getApplicationContext())) {
            // Retrieve product data from api and display popup
            ProductApiClient.getProductApiClient().getProduct(
                    identifier, new Callback<Product>() {
                        @Override
                        public void success(Product product, Response response) {
                            if (product.getInfo().getTotal_results() == 1) {
                                showResultsDialog(true,
                                        product.getResults().get(0).getTitle(), identifier);
                            } else {
                                showResultsDialog(false, "Item cannot be identified", "");
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            // Bad request, network errors etc
                            showMessageDialog("Unsupported Barcode");
                        }
                    }
            );
        } else {
            showMessageDialog("You need a working internet connection to continue");
        }
    }

    private void showResultsDialog(boolean success, String itemName, String identifier) {
        DialogFragment fragment = ScanResultsDialogFragment.newInstance(
                success, itemName, identifier, this);

        fragment.show(getActivity().getSupportFragmentManager(), "scan_results");
    }

    private void showMessageDialog(String message) {
        DialogFragment fragment = MessageDialogFragment.newInstance("Error", message, this);
        fragment.show(getActivity().getSupportFragmentManager(), "scan_results");
    }

    private void closeMessageDialog() {
        closeDialog("scan_results");
    }

    private void closeFormatsDialog() {
        closeDialog("format_selector");
    }

    private void closeDialog(String dialogName) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        DialogFragment fragment = (DialogFragment)
                fragmentManager.findFragmentByTag(dialogName);
        if (fragment != null) {
            fragment.dismiss();
        }
    }

    private void setupFormats() {
        List<BarcodeFormat> formats = new ArrayList<BarcodeFormat>();
        if (mSelectedIndices == null || mSelectedIndices.isEmpty()) {
            mSelectedIndices = new ArrayList<Integer>();
            for (int i = 0; i < BarcodeFormat.ALL_FORMATS.size(); i++) {
                mSelectedIndices.add(i);
            }
        }

        for (int index : mSelectedIndices) {
            formats.add(BarcodeFormat.ALL_FORMATS.get(index));
        }
        if (mScannerView != null) {
            mScannerView.setFormats(formats);
        }
    }

    @Override
    public void onScanResultDialogClick(DialogFragment dialog) {
        // Resume the camera
        mScannerView.startCamera();
        mScannerView.setFlash(mFlash);
        mScannerView.setAutoFocus(mAutoFocus);
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        // Resume the camera
        mScannerView.startCamera();
        mScannerView.setFlash(mFlash);
        mScannerView.setAutoFocus(mAutoFocus);
    }
}