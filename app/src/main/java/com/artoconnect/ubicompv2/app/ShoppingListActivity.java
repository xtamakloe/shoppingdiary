package com.artoconnect.ubicompv2.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.artoconnect.ubicompv2.adapters.MasterItemsListAdapter;
import com.artoconnect.ubicompv2.api.ApiUtilities;
import com.artoconnect.ubicompv2.api.UbicompApiClient;
import com.artoconnect.ubicompv2.models.MasterItem;
import com.artoconnect.ubicompv2.models.ShoppingList;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class ShoppingListActivity extends ActionBarActivity {
    private MasterItemsListAdapter mAdapter;
    private ListView mListView;
    private TextView mItemCount;
    private TextView mListName;
    private List<MasterItem> mItems;
    private Integer mListId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_list);

        mListName = (TextView) findViewById(R.id.tv_shopping_list_name_2);
        mItemCount = (TextView) findViewById(R.id.tv_lists_count);
        mListView = (ListView) findViewById(R.id.shoppingListsView);

        mItems = new ArrayList<MasterItem>();
        mAdapter = new MasterItemsListAdapter(this, mItems);
        mListView.setAdapter(mAdapter);

        // Get listId from Intent
        Intent intent = getIntent();
        mListId = intent.getIntExtra("listId", 0);

        loadItems(mListId);
    }

    private void loadItems(int listId) {
        // api call to get items and populate list
        if (ApiUtilities.isNetworkAvailable(getApplicationContext())) {
            UbicompApiClient.getUbicompApiClient().getList(
                    Globals.sUserId, listId,
                    new Callback<ShoppingList>() {
                        @Override
                        public void success(ShoppingList list, Response response) {
                            mListName.setText(list.getName());
                            mItemCount.setText("Number of Items: "
                                    + list.getItem_count().toString());

                            mItems.clear();
                            mItems.addAll(list.getMasterItems());
                            mAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                        }
                    }
            );
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.shopping_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_delete_list) {
            deleteList();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteList() {
        if (ApiUtilities.isNetworkAvailable(getApplicationContext())) {
            UbicompApiClient.getUbicompApiClient().
                    deleteList(Globals.sUserId, mListId, new Callback<Boolean>() {
                        @Override
                        public void success(Boolean aBoolean, Response response) {
                            Toast.makeText(getApplicationContext(),
                                    "Deleted",
                                    Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void failure(RetrofitError retrofitError) {
                            // TODO: Fix error here, list deleted but wrong response
                            Toast.makeText(getApplicationContext(),
                                    "Deleted",
                                    Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(),
                    "You need a working internet connection to continue",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
