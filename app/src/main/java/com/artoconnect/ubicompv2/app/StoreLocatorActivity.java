package com.artoconnect.ubicompv2.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.widget.Toast;

import com.artoconnect.ubicompv2.api.PlacesService;
import com.artoconnect.ubicompv2.models.Place;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;

public class StoreLocatorActivity extends FragmentActivity implements
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener,
        StoreLocatorDialog.StoreLocatorDialogListener,
        GoogleMap.OnInfoWindowClickListener {
    private final String TAG = getClass().getSimpleName();
    private LocationClient mLocationClient;
    private Location mCurrentLocation;
    private GoogleMap mMap;
    private ProgressDialog mProgressDialog;

    private HashMap<String, String> markerMap;

    @Override
    protected void onStart() {
        super.onStart();
        // Connect client here
        mLocationClient.connect();
    }

    @Override
    protected void onStop() {
        // Disconnect client here (invalidates it)
        mLocationClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_locator);
        mLocationClient = new LocationClient(this, this, this);

        try {
            mMap = ((MapFragment)
                    getFragmentManager().findFragmentById(R.id.map)).getMap();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /*
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     */
    @Override
    public void onConnected(Bundle bundle) {
        mCurrentLocation = mLocationClient.getLastLocation(); // last location

        // do connection centric work here
        if (mMap != null) {
            mMap.clear();
            new GetPlaces(StoreLocatorActivity.this, "store").execute();
        }
    }

    /*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onDisconnected() {
        Toast.makeText(this, "Disconnected. Please re-connect.",
                Toast.LENGTH_SHORT).show();
    }

    /*
     * Called by Location Services if the attempt to
     * Location Services fails.
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // Handle connection failure here.
        // Google Services can help see below.
    }

    @Override
    public void onStoreLocatorDialogClick(boolean success, int tripId) {

        if (success) {
            // Set current tripId
            Globals.sTripId = tripId;
            // TODO: incomplete implementation: handle success conditions, close map, cleanup etc
            Intent intent = new Intent(getApplicationContext(), BasketActivity.class);
            startActivity(intent);
            //startActivityForResult(intent, Globals.BASKET_ACTIVITY_REQUEST);
            finish();
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        FragmentManager fm = getSupportFragmentManager();
        DialogFragment dialog = StoreLocatorDialog.newInstance(
            this, marker.getTitle(), markerMap.get(marker.getId()),
                StoreLocatorActivity.this);
        dialog.show(fm, "store_locator");
    }

    private class GetPlaces extends AsyncTask<Void, Void, ArrayList<Place>> {

        // private ProgressDialog dialog;
        private Context context;
        private String place;

        public GetPlaces(Context context, String place) {
            this.context = context;
            this.place = place;
        }

        @Override
        protected void onPostExecute(ArrayList<Place> result) {
            super.onPostExecute(result);
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }

            markerMap = new HashMap<String, String>(); // TODO: clear or create new?

            for (int i = 0; i < result.size(); i++) {
                MarkerOptions options = new MarkerOptions();
                options.title(result.get(i).getName())
                        .position(new LatLng(result.get(i).getLatitude(),
                                result.get(i).getLongitude()))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin))
                        .snippet(result.get(i).getVicinity());

                // Add marker
                Marker marker = mMap.addMarker(options);

                // Save location's id with corresponding marker id
                markerMap.put(marker.getId(), result.get(i).getId());
            }

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(result.get(0).getLatitude(), result
                            .get(0).getLongitude())) // Sets the center of the map to
                            // Mountain View
                    .zoom(16) // Sets the zoom
                    .tilt(30) // Sets the tilt of the camera to 30 degrees
                    .build(); // Creates a CameraPosition from the builder

            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            mMap.getUiSettings().setCompassEnabled(true);
            mMap.setMyLocationEnabled(true);

            // Handle user selection
            mMap.setOnInfoWindowClickListener(StoreLocatorActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Loading..");
            mProgressDialog.isIndeterminate();
            mProgressDialog.show();
        }

        @Override
        protected ArrayList<Place> doInBackground(Void... arg0) {
            PlacesService service =
                    new PlacesService("AIzaSyCzs98wvrDie1veJyiYoEw9-b2ISRSjCLI");
            ArrayList<Place> findPlaces =
                    service.findPlaces(mCurrentLocation.getLatitude(),
                            mCurrentLocation.getLongitude(), place);
            return findPlaces;
        }
    }
}
