package com.artoconnect.ubicompv2.app;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.artoconnect.ubicompv2.api.ApiUtilities;
import com.artoconnect.ubicompv2.api.UbicompApiClient;
import com.artoconnect.ubicompv2.models.Trip;
import com.google.android.gms.maps.model.LatLng;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class StoreLocatorDialog extends DialogFragment {

    private StoreLocatorDialogListener mListener;
    private String mStoreName;
    private LatLng mStoreLocation;
    private String mStoreId;
//    private Context context;

    public static StoreLocatorDialog newInstance(
            Context context,
            String storeName, String storeId, StoreLocatorDialogListener listener) {
        StoreLocatorDialog fragment = new StoreLocatorDialog();

        fragment.mStoreName = storeName;
        fragment.mListener = listener;
        fragment.mStoreId = storeId;
//        fragment.context = context;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("Do you want to shop at " + mStoreName + "?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Create trip and return info to TripActivity to proceed
                        if (ApiUtilities.isNetworkAvailable(
                                getActivity().getApplicationContext())) {
                            UbicompApiClient.getUbicompApiClient().addTrip(
                                    Globals.sUserId, mStoreId, mStoreName,
                                    new Callback<Trip>() {
                                        @Override
                                        public void success(Trip trip, Response response) {
                                            // TODO: incomplete implementation
                                            // Return tripId
                                            if (mListener != null)
                                                mListener.onStoreLocatorDialogClick(
                                                        true, trip.getId());
                                        }

                                        @Override
                                        public void failure(RetrofitError error) {
                                            // TODO: incomplete implementation
                                            // Display error and return to map
                                            if (mListener != null)
                                                mListener.onStoreLocatorDialogClick(
                                                        true, 0);
                                        }
                                    }
                            );
                        }
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // TODO: complete implementation
                        if (mListener != null)
                            mListener.onStoreLocatorDialogClick(false, 0);
                    }
                });
        return builder.create();
    }

    public interface StoreLocatorDialogListener {
        public void onStoreLocatorDialogClick(boolean success, int tripId);
    }
}