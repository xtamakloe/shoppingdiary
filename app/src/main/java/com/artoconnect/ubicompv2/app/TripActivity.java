package com.artoconnect.ubicompv2.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.artoconnect.ubicompv2.adapters.ItemsListAdapter;
import com.artoconnect.ubicompv2.api.ApiUtilities;
import com.artoconnect.ubicompv2.api.UbicompApiClient;
import com.artoconnect.ubicompv2.models.Item;
import com.artoconnect.ubicompv2.models.Trip;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class TripActivity extends ActionBarActivity {
    private ItemsListAdapter mAdapter;
    private ListView mListView;
    private TextView mDuration;
    private TextView mItemCount;
    private TextView mStoreName;
    private TextView mTimeStamp;
    private List<Item> mItems;
    private Integer mTripId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip);

        mStoreName = (TextView) findViewById(R.id.tv_store_name);
        mDuration = (TextView) findViewById(R.id.tv_duration);
        mItemCount = (TextView) findViewById(R.id.tv_item_count);
        mTimeStamp = (TextView) findViewById(R.id.tv_timestamp);

        mListView = (ListView) this.findViewById(R.id.listView);

        mItems = new ArrayList<Item>();
        mAdapter = new ItemsListAdapter(this, mItems, false);
        mListView.setAdapter(mAdapter);

        // get tripId from intent
        Intent intent = getIntent();
        mTripId = intent.getIntExtra("tripId", 0);

        loadItems(mTripId);
    }

    private void loadItems(int tripId) {
        // api call to get items and populate list
        if (ApiUtilities.isNetworkAvailable(getApplicationContext())) {
            UbicompApiClient.getUbicompApiClient().getTrip(
                    Globals.sUserId, tripId,
                    new Callback<Trip>() {
                        @Override
                        public void success(Trip trip, Response response) {
                            mStoreName.setText(trip.getStore().getName());
                            mDuration.setText("Duration: " + trip.getDuration());
                            mItemCount.setText("Number of Items: "
                                    + trip.getItem_count().toString());
                            mTimeStamp.setText("Visited On " + trip.getTimestamp());

                            mItems.clear();
                            mItems.addAll(trip.getItems());
                            mAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                        }
                    }
            );
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.trip, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_delete_trip) {
            deleteTrip();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteTrip() {
        if (ApiUtilities.isNetworkAvailable(getApplicationContext())) {
            UbicompApiClient.getUbicompApiClient().
                    cancelTrip(Globals.sUserId, mTripId, new Callback<Boolean>() {
                        @Override
                        public void success(Boolean aBoolean, Response response) {
                            Toast.makeText(getApplicationContext(),
                                    "Deleted",
                                    Toast.LENGTH_SHORT).show();
                            finish();
                        }

                        @Override
                        public void failure(RetrofitError retrofitError) {
                            // TODO: Fix error here, trip deleted but wrong response
                            Toast.makeText(getApplicationContext(),
                                    "Deleted",
                                    Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
        } else {
            Toast.makeText(getApplicationContext(),
                    "You need a working internet connection to continue",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
