package com.artoconnect.ubicompv2.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.artoconnect.ubicompv2.adapters.TripsListAdapter;
import com.artoconnect.ubicompv2.api.ApiUtilities;
import com.artoconnect.ubicompv2.api.UbicompApiClient;
import com.artoconnect.ubicompv2.models.Trip;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class TripsFragment extends Fragment {
;
    private ListView mListView;
    private List<Trip> mTrips;
    private TripsListAdapter mAdapter;
    private ProgressBar mProgressBar;
    private TextView mEmptyView;

    public TripsFragment() {
        // Required empty public constructor
    }

    public static TripsFragment newInstance() {
        TripsFragment fragment = new TripsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_trips, container, false);

        mListView = (ListView) v.findViewById(R.id.lv_trips_list);
        mProgressBar = (ProgressBar) v.findViewById(R.id.progressbar);
        mEmptyView = (TextView) v.findViewById(R.id.empty_trips);

        mTrips = new ArrayList<Trip>();
        mAdapter = new TripsListAdapter(getActivity(), mTrips);
        mListView.setAdapter(mAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView,
                                    View view, int position, long row) {
                Intent intent = new Intent(getActivity(), TripActivity.class);
                Trip trip = (Trip) mAdapter.getItem(position);
                intent.putExtra("tripId", trip.getId());
                startActivity(intent);
            }
        });

        loadTrips();

        final ImageButton startTrip = (ImageButton) v.findViewById(R.id.btn_add_trip);
        startTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        getActivity().getApplicationContext(),
                        StoreLocatorActivity.class);
                startActivity(intent);
            }
        });

        return v;
    }

    @Override
    public void onResume() {
        loadTrips();
        super.onResume();
    }

    private void loadTrips() {
        // show progressbar
        mProgressBar.setVisibility(View.VISIBLE);

        // api call to get trips and populate listview
        if (ApiUtilities.isNetworkAvailable(getActivity().getApplicationContext())) {
            UbicompApiClient.getUbicompApiClient().getTrips(
                    Globals.sUserId, 100, 0,
                    new Callback<List<Trip>>() {
                        @Override
                        public void success(List<Trip> trips, Response response) {
                            // Hide progressbar
                            mProgressBar.setVisibility(View.GONE);

                            mTrips.clear();
                            mTrips.addAll(trips);
                            Globals.refreshView(mTrips, mListView, mEmptyView);
                            mAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                        }
                    }
            );
        }
    }
}
