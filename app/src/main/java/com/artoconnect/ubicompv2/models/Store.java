
package com.artoconnect.ubicompv2.models;

import java.util.HashMap;
import java.util.Map;

public class Store {

    private String uid;
    private String name;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
