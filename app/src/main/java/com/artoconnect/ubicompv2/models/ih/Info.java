
package com.artoconnect.ubicompv2.models.ih;

import java.util.HashMap;
import java.util.Map;

public class Info {

    private int start;
    private int total_results;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getTotal_results() {
        return total_results;
    }

    public void setTotal_results(int total_results) {
        this.total_results = total_results;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
