package com.artoconnect.ubicompv2.models.ih;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Result {

    private List<Object> isbns = new ArrayList<Object>();
    private int number_of_pages;
    private List<Object> asins = new ArrayList<Object>();
    private List<Object> mpns = new ArrayList<Object>();
    private List<Object> eans = new ArrayList<Object>();
    private String title;
    private List<Object> upcs = new ArrayList<Object>();
    private List<Object> models = new ArrayList<Object>();
    private List<Object> categories = new ArrayList<Object>();
    private String id;
    private String resource;
    private List<Object> brands = new ArrayList<Object>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public List<Object> getIsbns() {
        return isbns;
    }

    public void setIsbns(List<Object> isbns) {
        this.isbns = isbns;
    }

    public int getNumber_of_pages() {
        return number_of_pages;
    }

    public void setNumber_of_pages(int number_of_pages) {
        this.number_of_pages = number_of_pages;
    }

    public List<Object> getAsins() {
        return asins;
    }

    public void setAsins(List<Object> asins) {
        this.asins = asins;
    }

    public List<Object> getMpns() {
        return mpns;
    }

    public void setMpns(List<Object> mpns) {
        this.mpns = mpns;
    }

    public List<Object> getEans() {
        return eans;
    }

    public void setEans(List<Object> eans) {
        this.eans = eans;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Object> getUpcs() {
        return upcs;
    }

    public void setUpcs(List<Object> upcs) {
        this.upcs = upcs;
    }

    public List<Object> getModels() {
        return models;
    }

    public void setModels(List<Object> models) {
        this.models = models;
    }

    public List<Object> getCategories() {
        return categories;
    }

    public void setCategories(List<Object> categories) {
        this.categories = categories;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public List<Object> getBrands() {
        return brands;
    }

    public void setBrands(List<Object> brands) {
        this.brands = brands;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
